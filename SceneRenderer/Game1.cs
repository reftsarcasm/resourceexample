﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SceneRenderer
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Texture2D _background, _platform;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _background = Content.Load<Texture2D>("clouds");
            _platform = Content.Load<Texture2D>("bricks");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            RenderMap();
            spriteBatch.End();
            base.Draw(gameTime);
        }


        protected void RenderMap()
        {
            spriteBatch.Draw(_background, new Rectangle(0,0,800,600), null, Color.White);
            var platformPoints = new Point[]
            {
                new Point(0,440),
                new Point(40,440),
                new Point(80,440),
                new Point(120,440),
                new Point(160,440),
                new Point(200,440),
                new Point(240,440),
                new Point(280,440),
                new Point(320,440),
                new Point(360,440),
                new Point(400,440),
                new Point(440,440),
                new Point(480,440),
                new Point(520,440),
                new Point(560,440),
                new Point(600,440),
                new Point(640,440),
                new Point(680,440),
                new Point(720,440),
                new Point(760,440),
                new Point(800,440), 
                new Point(50, 200), 
                new Point(90, 200), 
                new Point(130, 200), 
                new Point(120, 300), 
                new Point(160, 300), 
                new Point(200, 300), 
                new Point(560, 300), 
                new Point(600, 300), 
                new Point(640, 300), 
                new Point(640, 150), 
                new Point(680, 150), 
                new Point(720, 150), 
            };
            foreach (var platformPoint in platformPoints)
            {
                spriteBatch.Draw(this._platform, new Rectangle(platformPoint.X, platformPoint.Y, _platform.Width, _platform.Height), null, Color.White);
            }
        }
    }
}
